// Script file for wordpress x3dom plugin

// Updates the height of x3d elements that have `aspectRatio` set
function x3dom_updateHeight ()
{
    jQuery("x3d[aspectRatio]").attr(
        "height",
        function(i, h)
        {
            return jQuery(this).width() / jQuery(this).attr("aspectRatio");
        });
};

// attach to events
jQuery(window).load(x3dom_updateHeight);
jQuery(window).resize(x3dom_updateHeight);
