=== X3DOM 3D Model Viewer ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: 
Tags: x3dom, 3d viewer, x3d, 3D model viewer, x3d viewer, 3d model display, 3D Model Viewer WordPress
Requires at least: 4.0.1
Tested up to: 4.3
Stable tag: 4.3
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A plugin to add x3dom support and 3d view in wordpress using shortcodes
Responsive Web Design
3D model Rotation, Panning and Zooming enabled.

== Description ==

An experimental plugin to add x3dom support and 3d view in wordpress using shortcodes
Responsive Web Design
Displays 3D model on wordPress page, post, or custom page
3D model Rotation, Panning and Zooming enabled
Background top and bottom colors

Plugin Features

* 3D model Display
* Rotate, Pan and Zooming enabled
* ShortCodes System
* Multiple 3D frames on the same Page or Post
* background top and bottom colors
* Very Lightweight

Live Preview: 

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/x3dom` directory, or Install as a regular WordPress plugin
2. Go your Pluings page via WordPress Dashboard and activate it
3. Use these shortcodes to post or page 

   `[x3d width=100% aspectRatio=1.33 file=id_nbr]`
   `[x3d width=500px height=400px file=id_nbr]`
   `[x3d file=id_nbr]`
4. Use x3d file as your model to be displayed, insert your 3d file ID number as assigned by wordpress
   when you upload your file
5. Use the Settings->X3DOM screen to configure the plugin
   * change wp_x3d.css to adapt some style aspect


Shortcode Parameters:

* width = any number i.e.: 90% for responsive frame or 500px for fixed width
* height = any number i.e.: 400px for fixed height
* omit width and height to get 100% responsive frame
* aspectRatio = any number i.e.: 1.33 
* change `wp_x3d.css` to adapt some style aspect (as background top and bottom colors)
* file = 3d model x3d source link id (ex: file=5)


== Frequently asked questions ==
Display 3D model on wordPress page, post, or custom page, 3D model Rotation, zooming enabled.

== Upgrade Notice ==

= 1.0 =
Basic version, still under development

== Screenshots ==

1. 3D model Display on Post
2. 3D model Display shortcodes


== Changelog ==

= 1.0 =
* Initial release