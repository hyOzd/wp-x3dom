<?php
/*
Plugin Name: X3DOM
Plugin URI:
Description: An experimental plugin to add x3dom support
Version:     1.0
Author:      Hasan Yavuz Özderya
Author URI:  https//hasanyavuz.ozderya.net
License:     GPL3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// register x3d scripts and styles
function x3dom_enqueue_scripts() {
    // wp_enqueue_script( 'x3dom-full-js', plugins_url('x3dom-full.js', __FILE__), false );
    // wp_enqueue_style( 'x3dom-style', plugins_url('x3dom.css', __FILE__) );
    wp_enqueue_script( 'x3dom-remote-js', 'http://www.x3dom.org/download/x3dom.js', false );
    wp_enqueue_style( 'x3dom-remote-style', 'http://www.x3dom.org/download/x3dom.css' );
    wp_enqueue_style( 'x3dom-wp-style', plugins_url('wp_x3d.css', __FILE__) );

    wp_register_script( 'x3dom-wp-js', plugins_url('wp_x3d.js', __FILE__), array('jquery'));
    wp_enqueue_script( 'x3dom-wp-js' );
}

add_action( 'wp_enqueue_scripts', 'x3dom_enqueue_scripts' );

/*
 * x3d shortcode generator.
 *
 * This shortcode requires 3 parameters.
 *
 * file : x3d file ID
 * width : width of the canvas (optional)
 * height : height of the canvas (optional)
 * aspectratio : height to width ratio of x3d element, ignored if `height` is provided
 */
function x3dom_shortcode($atts) {
    $x3dom_atts = shortcode_atts( array(
        'file' => '',
        'width' => '90%',
        'height' => '',
        'aspectratio' => '1.3307'
    ), $atts, 'x3d' );

    $file_id = $x3dom_atts[ 'file' ];
    $file_url = wp_get_attachment_url( $file_id );

    $width_style = "";
    $height_style = "";
    $aspectRatio_attr = "";

    if ($x3dom_atts['width'])
    {
        $width_style = "width:".$x3dom_atts['width'].";";
    }
    if ($x3dom_atts['height'])
    {
        $height_style = "height:".$x3dom_atts['height'].";";
    }
    else
    {
        $aspectRatio_attr = "aspectRatio='".$x3dom_atts[ 'aspectratio' ]."'";
    }

    return "<x3d file_id='$file_id' $aspectRatio_attr style='$width_style $height_style'>
<scene>
<inline url=\"$file_url\"> </inline>
</scene>
</x3d>";
}

// register shortcode
function x3dom_register_shortcode() {
    add_shortcode( 'x3d', 'x3dom_shortcode' );
}

add_action( 'init', 'x3dom_register_shortcode' );

// allow uploading files with .x3d extension
function x3dom_mimes( $existing_mimes = array() ) {
    $existing_mimes['x3d'] = 'model/x3d+xml';
    return $existing_mimes;
}

add_filter('upload_mimes', 'x3dom_mimes');

?>
